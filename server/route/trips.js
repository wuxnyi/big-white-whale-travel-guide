var express = require('express');

var db = require('../utils/db');  //数据库管理工具
var http = require('../utils/http'); //http 管理工具
var util = require('../utils/util'); //时间工具

//应用级中间件绑定到 app 对象 使用
var app = express();
const route = express.Router();


route.get('/:tripId/waypoints/', function (req, res) {
    var tripId = req.params.tripId;

    var trip = {};
    db.getTripById(tripId, async function (data) {
        if (data == null) {
            http.send(res, -1, "system error");
            return;
        }
        trip.user = {
            avatar_l: data.avatarUrl,
            name: data.nickName,
            id: data.user_id
        };
        trip.name = data.title;
        trip.first_day = data.start_date;
        trip.start_date = data.start_date;
        trip.end_date = data.end_date;
        trip.view_count = data.view_count;
        trip.recommendations = await db.getEnshrineCountByTripId2(tripId);
        var j = 0;
        trip.days = [];
        for (var i = trip.start_date; i <= trip.end_date; i += 24 * 60 * 60 * 1000) {
            var dayItem = {
                day: j + 1,
                date: util.formatTime(new Date(i), 3),
                waypoints: []
            }
            trip.days.push(dayItem);
            j++;
        }
        db.updateViewCount(tripId);

        db.getWayPointByTripId(tripId, function (data) {
            data.forEach(function (item, index, array) {
                var day = (item.trip_date - trip.first_day) / (24 * 60 * 60 * 1000);
                var wp = {
                    id: item.id,
                    photo: item.photo_webtrip,
                    photo_info: {
                        h: item.photo_info_h,
                        w: item.photo_info_w
                    },
                    photo_webtrip: item.photo_webtrip,
                    text: item.text,
                    local_time: util.formatTime(new Date(item.create_time), 0)
                };
                trip.days[day].waypoints.push(wp);
            });
            
            trip.first_day=trip.days[0].date;

            res.send(trip).end();
        });
    });


})
;


route.get('/:tripId/waypoints/:waypointId', function (req, res) {
    var tripId = req.params.tripId;
    var waypointId = req.params.waypointId;

    var waypoint = {};
    db.getWayPointByWayPointId(waypointId, function (item) {
        waypoint = {
            id: item.id,
            photo: item.photo_webtrip,
            photo_info: {
                h: item.photo_info_h,
                w: item.photo_info_w
            },
            photo_webtrip: item.photo_webtrip,
            text: item.text,
            local_time: util.formatTime(new Date(item.trip_date), 3)
        };
        db.getCommentsCountByWPId(waypointId, function (data) {
            waypoint.comments = data.count;
            res.send(waypoint).end();

        });
    });
})
;

route.get('/:tripId/waypoints/:waypointId/replies/', function (req, res) {
    var tripId = req.params.tripId;
    var waypointId = req.params.waypointId;

    var replies = {};
    db.getCommentsByWPId(waypointId, function (data) {
        replies.comment_count = data.length;
        replies.comments = [];
        data.forEach(function (item, index, array) {
            var comment = {
                id: item.id,
                user: {
                    avatar_m: item.avatarUrl,
                    userId: item.user_id,
                    name: item.nickName
                },
                comment: item.comments,
                date_added: item.create_time
            };
            replies.comments.push(comment);
        });
        db.getEnshrineByWPId(waypointId, function (data) {
            replies.recommender_count = data.length;
            replies.recommenders = [];
            data.forEach(function (item, index, array) {
                var user = {
                    avatar_m: item.avatarUrl,
                    id: item.user_id,
                    name: item.nickName
                };
                replies.recommenders.push(user);
            });
            res.send(replies).end();
        });
    });
})
;

//设置跨域访问
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", ' 3.2.1');
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});


module.exports = route;
